# coding:utf-8

import json

from django.db import transaction
from django.conf import settings
from django.core.exceptions import NON_FIELD_ERRORS, ValidationError
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseNotAllowed
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils.dateparse import parse_date

from booktable.models import Booking, Table
from booktable.forms import BookingForm


def index(request):
    table_qs = Table.objects.all()
    return render(request, 'booktable/index.html', {
        'tables': table_qs,
    })

def booking(request):
    if request.method == 'GET':
        try:
            date = parse_date(request.GET.get('date'))
            if not date:
                raise Exception()
        except:
            return HttpResponseBadRequest('Incorrect date')
        booking_qs = Booking.objects.filter(date=date)
        result = [ item.table.num for item in booking_qs ]
        return HttpResponse(json.dumps(result), content_type='application/json')

    elif request.method == 'POST':
        errors = {}

        with transaction.atomic():
            form = BookingForm(request.POST)
            if form.is_valid():
                book_list = []
                for tbl_id in form.cleaned_data['tables']:
                    book_list.append(Booking(table_id=tbl_id, date=form.cleaned_data['date'], email=form.cleaned_data['email']))
            try:
                Booking.objects.bulk_create(book_list)
                message_text = render_to_string('booktable/message.txt', context=form.cleaned_data)
                message_html = render_to_string('booktable/message.html', context=form.cleaned_data)
                send_mail(
                    subject=settings.BOOKTABLE_SUBJECT, 
                    message=message_text, 
                    from_email=settings.BOOKTABLE_EMAIL, 
                    recipient_list=[form.cleaned_data['email']], 
                    html_message=message_html
                )
            except:
                raise
            
            booking_new = []
            booking_qs = Booking.objects.filter(date=form.cleaned_data['date'])
            result = [ item.table.num for item in booking_qs ]
            return HttpResponse(json.dumps(result), content_type='application/json')

    else:
        return HttpResponseNotAllowed(['get', 'post'])
