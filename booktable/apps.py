from django.apps import AppConfig


class BooktableConfig(AppConfig):
    name = 'booktable'
