from django.contrib import admin

from booktable.models import Booking, Table


admin.site.register(Booking)
admin.site.register(Table)
