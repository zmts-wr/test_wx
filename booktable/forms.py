# coding:utf-8

import json

from django import forms

from booktable.models import Table

class TableListField(forms.CharField):
    def to_python(self, value):
        value = super().to_python(value)
        value = json.loads(value)
        # import pdb; pdb.set_trace()
        return value


class BookingForm(forms.Form):
    date = forms.DateField()
    name = forms.CharField()
    email = forms.EmailField()
    tables = TableListField()
