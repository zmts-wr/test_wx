# coding:utf-8
from django.core.exceptions import NON_FIELD_ERRORS, ValidationError
from django.db import models


class Table(models.Model):
    TABLE_FORMS = (
        ('rect', 'Rectangular'),
        ('elipse', 'Elipse'),
    )
    
    num = models.PositiveIntegerField(verbose_name='Number', unique=True)
    seats = models.PositiveIntegerField(verbose_name='Seats')
    form = models.CharField(verbose_name='Form', max_length=20, choices=TABLE_FORMS, default='rect')
    coord_x = models.IntegerField(verbose_name='Coordinate X')
    coord_y = models.IntegerField(verbose_name='Coordinate Y')
    size_x = models.IntegerField(verbose_name='Size H')
    size_y = models.IntegerField(verbose_name='Size V')
    
    def __str__(self):
        return '№ {} {}, for {} person(s)'.format(self.num, self.get_form_display(), self.seats)
    
    def clean(self):
        message = {
            'small': 'This table is too small',
            'big': 'This table is too big',
            'out': 'This table is out of room',
        }
        
        errors = {}
        
        if self.size_x < 0:
            errors['size_x'] = message['small']
        if self.size_x > 100:
            errors['size_x'] = message['big']
        if self.size_y < 0:
            errors['size_y'] = message['small']
        if self.size_y > 100:
            errors['size_y'] = message['big']
        
        if self.coord_x < 0:
            errors[NON_FIELD_ERRORS] = message['out']
        if self.coord_y < 0:
            errors[NON_FIELD_ERRORS] = message['out']
        
        if self.coord_x + self.size_x > 100:
            errors[NON_FIELD_ERRORS] = message['out']
        if self.coord_y + self.size_y > 100:
            errors[NON_FIELD_ERRORS] = message['out']
        
        if errors:
            raise ValidationError(errors)


class Booking(models.Model):
    table = models.ForeignKey(Table, verbose_name='Table', on_delete=models.CASCADE)
    date = models.DateField(verbose_name='Date')
    email = models.EmailField(verbose_name='Email')
    
    def __str__(self):
        return '{} @ {}'.format(self.table.num, self.date.strftime('%Y-%m-%d'))
    
    class Meta:
        unique_together = (('table', 'date',),)
