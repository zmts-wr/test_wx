from django.urls import include, path

from booktable import views

urlpatterns = [
    path('booking/', views.booking, name='booking'),
    path('', views.index, name='index'),
]