var app = angular.module('booktable',[]);

app.config(function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});

app.controller('RoomCtrl', function($scope, $filter, $http, $log, $httpParamSerializerJQLike, Tables) {
    $scope.selected = [];
    $scope.tables = Tables.data;
    $scope.date = new Date();
    $scope.f = {};
    
    $scope.$watch('date', function(newVal, oldVal) {
        $log.log('watch date', oldVal, newVal);
        $scope.get_booking(newVal);
    });
    $scope.update_table_state = function(booked) {
        for (var tbl in $scope.tables) {
            $scope.tables[tbl].free = (booked.indexOf($scope.tables[tbl].num) == -1);
        }
    };
    $scope.reset = function() {
        $scope.selected.splice(0, $scope.selected.length);
    };
    $scope.get_booking = function(date) {
        $scope.reset();
        $http({
            url: '/booking/',
            method: 'get',
            params: {
                date: $filter('date')(date, 'yyyy-MM-dd')
            }
        }).then(function(response) {
            $scope.update_table_state(response.data);
        }).catch(function() {
            //$scope.tables = [];
        });
    };
    $scope.set_booking = function() {
        $log.log('set booking', $scope.selected);
        $http({
            url: '/booking/',
            method: 'post',
            data: $httpParamSerializerJQLike({
                date: $filter('date')($scope.date, 'yyyy-MM-dd'),
                tables: JSON.stringify($scope.selected),
                name: $scope.f.name,
                email: $scope.f.email
            }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response) {
            $scope.update_table_state(response.data);
            $scope.reset();
        }).catch(function() {
            //$scope.tables = [];
        });
    };
    
    $scope.table_style = function(item) {
        var background = '#dddddd';
        if(item.free) {
            var n = $scope.selected.indexOf(item.num);
            background = n == -1 ? '#ffffff' : '#aaffaa';
        }
        var style = {
            top: item.coord_y+'%',
            left: item.coord_x+'%',
            height: item.size_y+'%',
            width: item.size_x+'%',
            background: background,
        };
        if(item.form == 'elipse') {
            style['border-radius'] = '50%';
        }
        return style;
    };
    
    $scope.click_table = function(item) {
        var n = $scope.selected.indexOf(item.num);
        if (n == -1) {
            if(item.free) {
                $scope.selected.push(item.num);
            }
        } else {
            $scope.selected.splice(n, 1);
        }
        $log.log('click_room selected', $scope.selected);
    };
});

app.directive('btDate', function() {
    return {
        restrict: 'EA',
        templateUrl: 'bt-date.tmpl',
        scope: {
            date: "="
        },
        link: function(scope, el, attrs) {
            scope.forth = function() {
                scope.date = new Date(scope.date.valueOf() + 24*60*60*1000);
            };
            scope.back = function() {
                scope.date = new Date(scope.date.valueOf() - 24*60*60*1000);
            };
        }
    };
});

app.service('Tables', function() {
    return {
        data: {}
    };
})